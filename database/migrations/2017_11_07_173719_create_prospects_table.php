<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProspectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('prospects', function (Blueprint $table) {
            $table->increments('id');
            $table->string('firstName', 50);
            $table->string('lastName', 50);
            $table->string('addressLine1', 80);
            $table->string('addressLine2', 80);
            $table->string('city', 50);
            $table->string('state', 50);
            $table->char('zip5', 5);
            $table->string('email', 80)->nullable();
            $table->string('requestEmailInfo', 1)->nullable();
            $table->timestamp('submitDate')->useCurrent();
            $table->string('landingPage', 80);
            $table->string('source', 50)->nullable();
            $table->string('campaignCode', 50);
            $table->string('campaignName', 50);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('prospects');
    }
}