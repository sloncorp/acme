<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the DatabaseSeeder seeds.
     *
     * @return void
     */
    public function run()
    {
        $sources = [
            [
                'vanityKeyword' => 'default',
                'campaignCode' => 'aep_nosource_2017',
                'campaignName' => 'No Source',
                'campaignPhone' => '1-800-312-1234'
            ],
            [
                'vanityKeyword' => 'Benefits',
                'campaignCode' => 'aep_preheatcontrol_2017',
                'campaignName' => 'Group 1',
                'campaignPhone' => '1-800-312-1235'
            ],
            [
                'vanityKeyword' => 'Care',
                'campaignCode' => 'aep_formerresponders_2017',
                'campaignName' => 'Group 2',
                'campaignPhone' => '1-800-312-1236'
            ],
            [
                'vanityKeyword' => 'Coverage',
                'campaignCode' => 'aep_rollfold_2017',
                'campaignName' => 'Group 3',
                'campaignPhone' => '1-800-312-1237'
            ],
            [
                'vanityKeyword' => 'Choices',
                'campaignCode' => 'aep_controltiming_2017',
                'campaignName' => 'Group 4',
                'campaignPhone' => '1-800-312-1238'
            ]
        ];

        DB::table('sources')->insert($sources);

        // US States

        $states = [
            ['name' => 'Alaska', 'code' => 'AK'],
            ['name' => 'Alabama', 'code' => 'AL'],
            ['name' => 'American Samoa', 'code' => 'AS'],
            ['name' => 'Arizona', 'code' => 'AZ'],
            ['name' => 'Arkansas', 'code' => 'AR'],
            ['name' => 'California', 'code' => 'CA'],
            ['name' => 'Colorado', 'code' => 'CO'],
            ['name' => 'Connecticut', 'code' => 'CT'],
            ['name' => 'Delaware', 'code' => 'DE'],
            ['name' => 'District of Columbia', 'code' => 'DC'],
            ['name' => 'Federated States of Micronesia', 'code' => 'FM'],
            ['name' => 'Florida', 'code' => 'FL'],
            ['name' => 'Georgia', 'code' => 'GA'],
            ['name' => 'Guam', 'code' => 'GU'],
            ['name' => 'Hawaii', 'code' => 'HI'],
            ['name' => 'Idaho', 'code' => 'ID'],
            ['name' => 'Illinois', 'code' => 'IL'],
            ['name' => 'Indiana', 'code' => 'IN'],
            ['name' => 'Iowa', 'code' => 'IA'],
            ['name' => 'Kansas', 'code' => 'KS'],
            ['name' => 'Kentucky', 'code' => 'KY'],
            ['name' => 'Louisiana', 'code' => 'LA'],
            ['name' => 'Maine', 'code' => 'ME'],
            ['name' => 'Marshall Islands', 'code' => 'MH'],
            ['name' => 'Maryland', 'code' => 'MD'],
            ['name' => 'Massachusetts', 'code' => 'MA'],
            ['name' => 'Michigan', 'code' => 'MI'],
            ['name' => 'Minnesota', 'code' => 'MN'],
            ['name' => 'Mississippi', 'code' => 'MS'],
            ['name' => 'Missouri', 'code' => 'MO'],
            ['name' => 'Montana', 'code' => 'MT'],
            ['name' => 'Nebraska', 'code' => 'NE'],
            ['name' => 'Nevada', 'code' => 'NV'],
            ['name' => 'New Hampshire', 'code' => 'NH'],
            ['name' => 'New Jersey', 'code' => 'NJ'],
            ['name' => 'New Mexico', 'code' => 'NM'],
            ['name' => 'New York', 'code' => 'NY'],
            ['name' => 'North Carolina', 'code' => 'NC'],
            ['name' => 'North Dakota', 'code' => 'ND'],
            ['name' => 'Northern Mariana Islands', 'code' => 'MP'],
            ['name' => 'Ohio', 'code' => 'OH'],
            ['name' => 'Oklahoma', 'code' => 'OK'],
            ['name' => 'Oregon', 'code' => 'OR'],
            ['name' => 'Palau', 'code' => 'PW'],
            ['name' => 'Pennsylvania', 'code' => 'PA'],
            ['name' => 'Puerto Rico', 'code' => 'PR'],
            ['name' => 'Rhode Island', 'code' => 'RI'],
            ['name' => 'South Carolina', 'code' => 'SC'],
            ['name' => 'South Dakota', 'code' => 'SD'],
            ['name' => 'Tennessee', 'code' => 'TN'],
            ['name' => 'Texas', 'code' => 'TX'],
            ['name' => 'Utah', 'code' => 'UT'],
            ['name' => 'Vermont', 'code' => 'VT'],
            ['name' => 'Virgin Islands', 'code' => 'VI'],
            ['name' => 'Virginia', 'code' => 'VA'],
            ['name' => 'Washington', 'code' => 'WA'],
            ['name' => 'West Virginia', 'code' => 'WV'],
            ['name' => 'Wisconsin', 'code' => 'WI'],
            ['name' => 'Wyoming', 'code' => 'WY'],
            ['name' => 'Armed Forces Africa', 'code' => 'AE']
        ];

        DB::table('states')->insert(
            $states
        );
    }
}