@extends('layout.index', ['campaignPhone' => $campaignPhone])

@section('content')

    <div class="row no-gutters flexthis">

        <div class="col-xs-12 col-sm-12 col-md-8 senior-care-container text-center">
            @include('landing.partials.senior_care')
        </div>

        <div class="col-xs-12 col-sm-12 col-md-4 form-container">
            @include('landing.partials.thank_you_message')
        </div>

    </div>

@endsection