<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">

<head>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

    <!--[if !mso]><!-->
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <!--<![endif]-->

    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title></title>

    <!--[if (gte mso 9)|(IE)]>
    <style type="text/css">

        table {
            border-collapse: collapse;
        }

    </style>
    <![endif]-->

    <style type="text/css">

        body {
            margin: 0 !important;
            padding: 0;
            background-color: #ffffff;
        }

        table {
            border-spacing: 0;
            font-family: sans-serif;
            color: #979797;
        }

        td {
            padding: 0;
        }

        img {
            border: 0;
        }

        div[style*="margin: 16px 0"] {
            margin: 0 !important;
        }

        .wrapper {
            width: 100%;
            table-layout: fixed;
            -webkit-text-size-adjust: 100%;
            -ms-text-size-adjust: 100%;
        }

        .webkit {
            max-width: 600px;
            margin: 0 auto;
        }

        .outer {
            Margin: 0 auto;
            width: 100%;
            max-width: 600px;
        }

        p {
            Margin: 0;
        }

        a {
            text-decoration: underline;
        }

        .center {
            text-align: center;
        }

    </style>

</head>

<body>

<center class="wrapper">

    <div class="webkit">

        <!--[if (gte mso 9)|(IE)]>
        <table width="600" align="center">
            <tr>
                <td>
        <![endif]-->

        <table class="outer" align="center">

            <tr class="center">
                <td>
                    <div style="width: 100%; max-width: 600px; height: auto; margin-top: 10px; margin-bottom: 5px; font-size: 13px; color: #979797">
                        Having trouble seeing this email? <a target="_blank" href="{{ route('email.request_received') }}">View</a> it in
                        your browser
                    </div>
                </td>
            </tr>

            <tr>
                <td class="non-full-width-image">
                    <img src="{{ asset('assets/images/logo_top.png') }}" width="300" alt=""
                         style="width: 45%; max-width: 600px; height: auto; margin-bottom: 20px;"/>
                </td>
            </tr>

            <tr>
                <td class="full-width-image">
                    <img src="{{ asset('assets/images/email_banner.png') }}" width="600" alt=""
                         style="width: 100%; background: #122c50; padding-top: 15px; height: auto;"/>
                </td>
            </tr>

            <tr class="center">
                <td>
                    <div style="color: #ff7f21; margin-top:25px; margin-bottom:25px; font-size: 24px; text-align: center;">
                        <b>Thank you,</b> we've received you request!
                    </div>
                </td>
            </tr>

            <tr class="center">
                <td>
                    <div style="padding-left: 30px; padding-right: 30px; text-align: center;">
                        Click the button below to download your FREE Medicare Decision Guide. If you have immediate
                        questions, please feel free to contact one of our licensed insurance agents at.
                    </div>
                </td>
            </tr>

            <tr class="center">
                <td>
                    <div style="background-color: #0d5a94; width: 30%; padding-top: 10px; padding-bottom: 10px; margin-top:30px; margin-bottom:30px; margin-left: 35%; text-align: center;">
                        <a target="_blank" href="{{ route('fake.guide') }}" class="download-button"
                           style="text-decoration: none; cursor:pointer; color: #FFFFFF;">
                            DOWNLOAD GUIDE
                        </a>
                    </div>
                </td>
            </tr>

            <tr class="center">
                <td>
                    <div style="background-color: #122c50; text-align: center; font-size: 12px; color: #FFFFFF; margin-bottom: 20px; padding: 20px 10px;">
                        &copy; 2017 Health Plan
                    </div>
                </td>
            </tr>

            <tr class="center">
                <td>
                    <div style="text-align: center; font-size: 13px;">
                        1206 West Campus Drive - Temple, Texas 76502.
                    </div>
                </td>
            </tr>

            <tr class="center">
                <td>
                    <div style="font-size: 13px;">
                        <a target="_blank" href="{{ route('fake.unsubscribe') }}" style="text-decoration: none; color: #979797;">unsubscribe</a>
                    </div>
                </td>
            </tr>

        </table>

        <!--[if (gte mso 9)|(IE)]>
                </td>
            </tr>
        </table>
        <![endif]-->

    </div>

</center>

</body>

</html>