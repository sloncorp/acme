<div class="text-center">

    <h3>{{ trans('app.content.form.title') }}</h3>

    <h5>{{ trans('app.content.form.message') }}</h5>

    @if ($errors->any())
        <div class="alert alert-danger">
            {{ trans('app.content.form.errors') }}
        </div>
    @endif
</div>

<form id="storeLandingForm" action="{{ route('store.landing') }}" method="post" autocomplete="off" novalidate>

    {{ csrf_field() }}

    <div class="form-group">
        <input id="firstName" name="firstName" class="form-control"
               placeholder="{{ trans('app.form.placeholders.firstName') }}" maxlength="50" required>
    </div>

    <div class="form-group">
        <input id="lastName" name="lastName" class="form-control"
               placeholder="{{ trans('app.form.placeholders.lastName') }}" maxlength="50" required>
    </div>

    <div class="form-group">
        <input id="addressLine1" name="addressLine1" class="form-control"
               placeholder="{{ trans('app.form.placeholders.addressLine1') }}" maxlength="80" required>
    </div>

    <div class="form-group">
        <input id="addressLine2" name="addressLine2" class="form-control"
               placeholder="{{ trans('app.form.placeholders.addressLine2') }}" maxlength="80" required>
    </div>

    <div class="form-group">
        <input id="city" name="city" class="form-control" placeholder="{{ trans('app.form.placeholders.city') }}"
               maxlength="50" required>
    </div>

    <div class="form-group">
        <select id="state" name="state" class="form-control" required>
            <option value="" class="select-state-text"> {{ trans('app.form.placeholders.state') }} </option>
            @foreach($states as $key => $state)
                <option value="{{ $key }}">{{ $state->name }}</option>
            @endforeach
        </select>
    </div>

    <div class="form-group">
        <input id="zip5" name="zip5" class="form-control w-50" maxlength="5" min="0" inputmode="numeric" pattern="[0-9]*"
               placeholder="{{ trans('app.form.placeholders.zip5') }}" required>
    </div>

    <div class="form-group d-flex no-gutters">
        <div class="form-check col-1">
            <input class="form-check-input" type="checkbox" id="requestEmailInfo" name="requestEmailInfo">
        </div>
        <div class="col-11">
            <p class="text-left"> {{ trans('app.form.placeholders.requestEmailInfo') }} </p>
            <input type="email" id="email" name="email" class="form-control"
                   placeholder="{{ trans('app.form.placeholders.email') }}" maxlength="80"
                   readonly>
        </div>
    </div>

    <input type="hidden" id="source" name="source" value="{{ $source->vanityKeyword }}">

    <input type="hidden" id="campaignName" name="campaignName" value="{{ $source->campaignName }}">

    <input type="hidden" id="campaignCode" name="campaignCode" value="{{ $source->campaignCode }}">

    <input type="hidden" id="campaignPhone" name="campaignPhone" value="{{ $source->campaignPhone }}">

    <input type="hidden" id="landingPage" name="landingPage" value="{{ $landingPage }}">

    <div class="form-group  ">
        <div class="ml-1 mr-1 mt-3">
            <button id="submitButton" class="btn btn-default btn-block">{{ trans('app.labels.submit') }}</button>
        </div>
    </div>

</form>

<script>

    $(function () {

        $("#requestEmailInfo").change(function () {
            if (this.checked) {
                $("#email").prop({
                    placeholder: '{{ trans('app.form.placeholders.email').' *' }}',
                    readonly: false,
                    required: true
                });
            }
            else {
                var $email = $("#email");
                $email.prop({
                    placeholder: '{{ trans('app.form.placeholders.email') }}',
                    readonly: true,
                    required: false
                });
                $email.val('');
                $email.siblings('label').remove();
            }
        });

        $("#zip5").keyup(function () {
            $("#zip5").val(this.value.match(/[0-9]*/));
        });

        $.validator.addMethod("zipcodeUS", function (value, element) {
            return this.optional(element) || /\d{5}-\d{4}$|^\d{5}$/.test(value)
        }, '{{ trans('app.form.errors.zip5invalid') }}');

        $("#storeLandingForm").validate({
            normalizer: function (value) {
                // Trim the value of every element
                return $.trim(value);
            },
            submitHandler: function (form) {
                $("#submitButton").prop('disabled', true);
                form.submit();
            },
            onfocusout: function (element) {
                this.element(element);
            },
            rules: {
                firstName: {
                    required: true,
                    maxlength: 50
                },
                lastName: {
                    required: true,
                    maxlength: 50
                },
                addressLine1: {
                    required: true,
                    maxlength: 80
                },
                addressLine2: {
                    required: true,
                    maxlength: 80
                },
                city: {
                    required: true,
                    maxlength: 50
                },
                state: {
                    required: true,
                    maxlength: 50
                },
                zip5: {
                    required: true,
                    zipcodeUS: true,
                    maxlength: 5
                }

            },
            messages: {
                firstName: {
                    required: '{!! trans('app.form.errors.firstName') !!}'
                },
                lastName: {
                    required: '{!! trans('app.form.errors.lastName') !!}'
                },
                addressLine1: {
                    required: '{!! trans('app.form.errors.addressLine1') !!}'
                },
                addressLine2: {
                    required: '{!! trans('app.form.errors.addressLine2') !!}'
                },
                city: {
                    required: '{!! trans('app.form.errors.city') !!}'
                },
                state: {
                    required: '{!! trans('app.form.errors.state') !!}'
                },
                zip5: {
                    required: '{!! trans('app.form.errors.zip5') !!}'
                },
                email: {
                    required: '{!! trans('app.form.errors.email') !!}'
                }
            }
        });

    });

</script>

