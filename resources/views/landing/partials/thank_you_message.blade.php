<h2>{{ trans('app.content.thank_you.title') }}</h2>

<p>{{ trans('app.content.thank_you.message_1') }} <br/> {{ trans('app.content.thank_you.message_2') }}</p>

<p>
    <a href="http://www.sourcelink.com/customer-intelligence"
       target="_blank">{{ trans('app.content.thank_you.discover_element')}} </a> {{ trans('app.content.thank_you.discover_text') }}
</p>

<p>
    <a href="http://www.sourcelink.com/strategy-creative/"
       target="_blank">{{ trans('app.content.thank_you.get_help_element')}} </a> {{ trans('app.content.thank_you.get_help_text') }}
</p>

<p>
    <a href="http://www.sourcelink.com/digital-solutions/"
       target="_blank">{{ trans('app.content.thank_you.learn_more_element')}} </a> {{ trans('app.content.thank_you.learn_more_text') }}
</p>

<p>
    <a href="http://www.sourcelink.com/document-statement/"
       target="_blank">{{ trans('app.content.thank_you.contact_element') }} </a> {{ trans('app.content.thank_you.contact_text') }}
</p>