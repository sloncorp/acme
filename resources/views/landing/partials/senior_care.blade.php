<img src="{{ asset('assets/images/banner.png') }}" class="img-fluid w-100">

<div class="d-flex justify-content-center">

    <div class="senior-care-text text-center col-xs-12 col-sm-12 col-md-10 col-lg-9">

        <p class="senior-care-title">{{ trans('app.content.senior_care.title') }}</p>

        <p class="senior-care-subtitle">{{ trans('app.content.senior_care.sub_title') }}</p>

        <p class="senior-care-message ">
            {{ trans('app.content.senior_care.message') }}
        </p>

    </div>

</div>







