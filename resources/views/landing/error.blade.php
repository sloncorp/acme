@extends('layout.index', ['campaignPhone' => $campaignPhone ])

@section('content')
    <div class="row no-gutters">
        <div class="col-xs-12 col-sm-12 col-md-12 text-center error-generic">
            {{ trans('app.content.layout.error') }}
        </div>
    </div>
@endsection