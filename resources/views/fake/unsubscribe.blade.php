@extends('layout.index', ['campaignPhone' => '1800-1234-1234' ])

@section('content')
    <div class="row no-gutters">
        <div class="col-xs-12 col-sm-12 col-md-12 text-center error-generic">
            {{ trans('app.fake.unsubscribe') }}
        </div>
    </div>
@endsection