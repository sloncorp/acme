<div class="row header flexthis">
    <div class="col-xs-12 col-sm-6 d-flex justify-content-center justify-content-md-start">
        <img src="{{ asset('assets/images/logo_top.png') }}" class="img-fluid">
    </div>

    <div class="col-xs-12 col-sm-6 d-flex align-items-center justify-content-center justify-content-md-end mt-1 mt-sm-0">
        <div class="text-center">
            <div class="contact-an-agent">{{trans('app.content.layout.contact_agent')}}</div>
            <div class="contact-phone">{{ $campaignPhone }}</div>
        </div>
    </div>
</div>
