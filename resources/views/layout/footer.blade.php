<div class="row flexthis">
    <div class="col-xs-12 col-sm-6 d-flex justify-content-center justify-content-md-start mt-1 mt-sm-0">
        <img src="{{ asset('assets/images/logo_bottom.png') }}" class="img-fluid ">
    </div>

    <div class="col-xs-12 col-sm-6 d-flex align-items-center justify-content-center justify-content-md-end mt-1 mt-sm-0">
        <img src="{{ asset('assets/images/part_of.png') }}" class="img-fluid">
    </div>
</div>

<div class="row justify-content-center mt-1 mb-1">
    <div>
        <span class="copyright-text">&copy; {{trans('app.content.layout.year')}} {{trans('app.labels.owner')}} {{trans('app.labels.system_title')}}</span>
    </div>
</div>


