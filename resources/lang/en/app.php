<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Message Response Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default messages used by
    | the controller for response.
    |
    */

    'labels' => [
        'system_title' => 'Health Plan',
        'owner' => 'ACME',
        'submit' => 'Submit'
    ],

    'form' => [

        'placeholders' => [
            'firstName' => 'First Name *',
            'lastName' => 'Last Name *',
            'addressLine1' => 'Address Line 1 *',
            'addressLine2' => 'Address Line 2 *',
            'city' => 'City *',
            'state' => 'Select State *',
            'zip5' => 'Zip Code *',
            'email' => 'Email',
            'requestEmailInfo' => 'Yes, I\'d like to receive an email confirmation that my request has been processed as well as future information about
                Medicare.',
        ],

        'errors' => [
            'firstName' => 'First Name is required',
            'lastName' => 'Last Name is required',
            'addressLine1' => 'Address Line 1 is required',
            'addressLine2' => 'Address Line 2 is required',
            'city' => 'City is required',
            'state' => 'State is required',
            'zip5' => 'Zip Code is required',
            'email' => 'Email is required',
            'zip5invalid' => 'The specified US ZIP Code is invalid'
        ]
    ],

    'content' => [

        'form' => [
            'title' => 'I\'m ready to learn more.',
            'message' => 'Make sure the information below is complete and correct',
            'errors' => 'Please fill in the required fields',
        ],

        'senior_care' => [
            'title' => 'SeniorCare (Cost)',
            'sub_title' => 'Medicare HMO Plan',
            'message' => 'Frequent doctor visits really add up, especially those pesky out-of-pocket expenses. So enhance your Original Medicare benefits with SeniorCare, the only Cost plan in Texas. SeniorCare will cover all your Medicare deductibles and ensure minimal copays for doctor visits when you use network providers.',
        ],

        'thank_you' => [
            'title' => 'We\'ve got great news!',
            'message_1' => 'Thank you for submitting your information!',
            'message_2' => ' Your FREE 2017 Medicare Decision Guide will be on its way shortly.',
            'discover_element' => 'Discover',
            'discover_text' => 'the ACME Health Plan difference.',
            'get_help_element' => 'Get help',
            'get_help_text' => 'understanding Medicare.',
            'learn_more_element' => 'Learn more',
            'learn_more_text' => 'about our plans & rates.',
            'contact_element' => 'Contact',
            'contact_text' => 'your local Scott & White Health Plan agent.',
        ],

        'layout' => [
            'contact_agent' => 'CONTACT AN AGENT',
            'year' => '2017',
            'error' => 'We are sorry, something went wrong!.'
        ]
    ],

    'fake' => [
        'unsubscribe' => 'User will be able to unsubscribe here!',
        'guide' => 'User will be able to download the guide here!'
    ]

];
