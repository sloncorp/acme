const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

// html5shiv
mix.copy('node_modules/html5shiv/dist', 'public/assets/vendor/html5shiv', false);

// respond.js
mix.copy('node_modules/respond.js/dest', 'public/assets/vendor/respond', false);

// bootstrap
mix.copy('node_modules/bootstrap/dist', 'public/assets/vendor/bootstrap', false);

// jquery
mix.copy('node_modules/jquery/dist', 'public/assets/vendor/jquery', false);

// jquery validations
mix.copy('node_modules/jquery-validation/dist', 'public/assets/vendor/jquery-validation', false);

// images
mix.copy('resources/assets/images', 'public/assets/images', false);

// fonts
mix.copy('resources/assets/fonts', 'public/assets/fonts', false);

// custom css
mix.copy('resources/assets/css/custom.css', 'public/assets/css', false);

// app styles
mix.sass('resources/assets/sass/app.scss', 'public/assets/css');

// app scripts
mix.js('resources/assets/js/app.js', 'public/assets/js');