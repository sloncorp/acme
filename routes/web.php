<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::redirect('/', '/ACME', 301);

Route::get('/fake/unsubscribe', function(){
    return view('fake.unsubscribe');
})->name('fake.unsubscribe');

Route::get('/fake/guide', function(){
    return view('fake.guide');
})->name('fake.guide');

Route::get('/email/request_received', function(){
    return view('landing.email.request_received');
})->name('email.request_received');

Route::group([
    'prefix' => 'ACME'
], function () {
    Route::get('{source?}', 'LandingController@index')->name('index.landing');
    Route::post('/', 'LandingController@store')->name('store.landing');
});

Route::group([
    'prefix' => 'report',
], function () {
    Route::get('/generate', 'ReportController@generate')->name('report.generate');
});

