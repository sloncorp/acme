# ACME Health Care Landing Page

Landing Page Link [http://acme.todocloud.com/](http://acme.todocloud.com/)

## Prospect's Report

The prospect's report can be found at [http://acme.todocloud.com/report/generate](http://acme.todocloud.com/report/generate)

## Source Code 

* The source code can be found [here](https://bitbucket.org/sloncorp/acme/get/master.zip)
* Database model can be found [here](http://acme.todocloud.com/db_schema.png)
* Database SQL can be found [here](http://acme.todocloud.com/db_sql.sql)

## Considerations, assumptions, security measures

### Assumptions
The following assumptions have been taken into account:

* The name of the Vanity keyword is not case-sensitive, i.e "Choices". If user types "choices" it will work as "Choices" too.
* Any other word will be taken as a default word, thus, leading to `http://acme.todocloud.com/ACME/`
* Zip Code has been assumed as a 5 digit field.
* In the report's name, the format `TT` has been considered as the `Hour` of report. `UTC` has been used as the time zone.
* Responsive design and layouts for the different viewports and devices has been assumed since I did not receive any specific designs.

### Security Measures
Taking into account this project is Web-Based, I have taken this security measures.

* SQL Injection is a common exploit. CSRF and XSS are as well know types of attacks in a Web environment.
* One of the reasons I worked it with Laravel, is because it prevents this attacks out-of-the-box. So I would not have to reinvent the wheel.
* A captcha would be a great security measure to prevent the attack of a robot, but I omitted it as the design did not take it into consideration.
* Using `HTTPS` and/or a `SSL Certificate` is not strictly necessary in this case since we are not asking for credit card information, credentials or any sort of sensitive data.

### Methodology and Framework
* SCRUM has been taken as the development methodology. Consisted of 1 sprint.
* This project has been created on top of **[Laravel](https://laravel.com/docs/5.5/) 5.5 Framework** in order to leverage the DRY (Do not Repeat Yourself) philosophy. 

### Other Considerations
* In regard to the email, I have tested it on Outlook (Web Client) using Mac OS and Windows 8+. However, it is also working on other clients such as Gmail.
* I did not use litmus to test emails since it does not provide a preview for Outlook for free. However, I did a thorough testing in different Web Browsers.
* Database has been created using Laravel migrations, which makes it easy to maintain.
  
### Frontend
The following tools have been used on the frontend:

* Bootstrap 4. This library allows make the site responsive.
* Jquery. To manipulate the DOM and do form validations.
* NPM. In order to maintain and download official repositories for third party tools.

### Backend 
The following tools have been used on the backend (a LAMP stack):

* **L**inux AMI for AWS (Amazon Web Services)
* **A**pache 2.4.*
* **M**ySQL 5.7.*
* **P**HP 7.0.*

### Deployment Tools
The following tools have been used to deploy:

* This project has been deployed into an Amazon EC2 instance (linux based).
* The source control tool is GIT.
* Project Management System was [Trello](trello.com) (I did not see the need for a more complete PM tool like JIRA).
* WebPack. In order to package assets and minimize them.
* Aglio. This Readme file, was generated with Aglio. This tool interprets `MD files` and transforms them to HTML. 

## Installation Steps:

* Requirements: PHP 7.0.*, MySQL 5.7.*, Apache 2.4.*

1. Install backend dependencies
    * `composer install --prefer-dist`

2. Configure parameters
    * Copy `.env.example` to `.env` and update the parameters.
    * Create the database schema. Call it `acme`.

3. Generate laravel token key
    * `php artisan key:generate`

4. Install frontend dependencies 
    * `npm install --save`

5. Generate public assets
    * `npm run dev`

6. Execute database migrations to create tables and relationships.
    * `php artisan migrate --path database/migrations`
    
8. Fill basic data into the database
    * `php artisan db:seed  --class=DatabaseSeeder`