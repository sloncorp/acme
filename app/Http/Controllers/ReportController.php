<?php

namespace App\Http\Controllers;

use App\Models\Prospect;
use Carbon\Carbon;
use Illuminate\Routing\Controller as BaseController;
use Excel;

/**
 * Class ReportController
 * @package App\Http\Controllers
 */
class ReportController extends BaseController
{
    /**
     * Generates the prospect's report
     *
     * @return mixed
     */
    public function generate()
    {
        $prospects = Prospect::all();

        $filename = 'ACME_Website_Traffic_' . Carbon::now()->format('dmYHis');

        return Excel::create($filename, function ($excel) use ($prospects) {
            $excel->sheet('Sheet1', function ($sheet) use ($prospects) {
                $sheet->fromArray($prospects);
            });
        })->download('txt');
    }
}
