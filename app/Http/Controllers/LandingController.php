<?php

namespace App\Http\Controllers;

use App\Models\Prospect;
use App\Models\Source;
use App\Models\State;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Mail;
use \Validator;

/**
 * Class LandingController
 * @package App\Http\Controllers
 */
class LandingController extends BaseController
{
    /**
     * Shows the landing page
     *
     * @param string $source
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function index($source = 'default', Request $request)
    {
        try {

            $source = Source::where('vanityKeyword', $source)->firstOrFail();

            $states = State::all()->keyBy('code');

            return view('landing.index', [
                'source' => $source,
                'landingPage' => $request->fullUrl(),
                'states' => $states
            ]);
        } catch (ModelNotFoundException $exception) {

            return redirect(route('index.landing'));
        } catch (\Exception $exception) {

            return view('landing.error', [
                'campaignPhone' => ''
            ]);
        }
    }

    /**
     * Stores the prospect form
     *
     * @param Request $request
     * @return $this|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function store(Request $request)
    {
        try {

            $data = $request->all();

            $validator = Validator::make($data, [
                'firstName' => 'required|max:50',
                'lastName' => 'required|max:50',
                'addressLine1' => 'required|max:80',
                'addressLine2' => 'required|max:80',
                'city' => 'required|max:50',
                'state' => 'required|max:50',
                'zip5' => 'required|max:5'
            ]);

            if ($validator->fails()) {

                return redirect(route('index.landing'))
                    ->withErrors($validator)
                    ->withInput();
            }

            $data['source'] = $data['source'] == 'default' ? '' : $data['source'];

            $data['requestEmailInfo'] = isset($data['requestEmailInfo']) && $data['requestEmailInfo'] == 'on' ? 'Y' : '';

            $prospect = new Prospect();

            $prospect->fill($data);

            $prospect->save();

            if ($prospect->email && $prospect->requestEmailInfo == 'Y') {
                Mail::send('landing.email.request_received', [], function ($mail) use ($prospect) {
                    $mail->from(env('MAIL_USERNAME'), env('MAIL_FROM'));

                    $mail->to($prospect->email, $prospect->fullName())->subject('ACME Health Plan - Request Received');
                });
            }

            return view('landing.thank_you', [
                'campaignPhone' => $data['campaignPhone']
            ]);
        } catch (\Exception $exception) {

            return view('landing.error', [
                'campaignPhone' => ''
            ]);
        }
    }
}
