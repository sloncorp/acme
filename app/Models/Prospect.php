<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Prospect extends Model
{
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'firstName', 'lastName', 'addressLine1', 'addressLine2', 'city',
        'state', 'zip5', 'email', 'requestEmailInfo',
        'landingPage', 'source', 'campaignCode', 'campaignName'
    ];

    /**
     * Full name of the prospect
     *
     * @return string
     */
    public function fullName() {
        return $this->firstName . ' ' . $this->lastName;
    }
}